//
//  CellData.swift
//  CV
//
//  Created by Emil Christoffersson on 2019-12-06.
//  Copyright © 2019 Emil Christoffersson. All rights reserved.
//


import UIKit
import Foundation
class CellData {
    
    private var title: String
    private var date: String
    private var image: UIImage?
    
    
    init(title: String, date: String, image: UIImage?) {
        self.title = title
        self.date = date
        if image != nil{
            self.image = image
        }
    }
    
    func titleInCell() -> String {
        return self.title
    }
    func dateInCell() -> String {
        return self.date
    }
    func imageInCell() -> UIImage? {
        return self.image
    }
    
    
    func descriptionInCell() -> String {
        switch self.title {
        case "MIO Möbler":
            return """
            "Chocolate bar powder gummies chocolate cake chocolate pie donut. Wafer halvah candy dragée sweet roll topping powder. Fruitcake tootsie roll bonbon toffee gummies donut cake tiramisu wafer. Bear claw tiramisu apple pie tart jelly beans cupcake. Chupa chups icing candy canes. Liquorice muffin jelly beans ice cream marshmallow. Pastry halvah topping liquorice. Candy cheesecake jujubes. Cake chocolate cake lemon drops. Cupcake soufflé sweet jelly caramels jelly jelly-o"
            """
        case "Ystad Saltsjöbad":
            return """
            "Chocolate bar powder gummies chocolate cake chocolate pie donut. Wafer halvah candy dragée sweet roll topping powder. Fruitcake tootsie roll bonbon toffee gummies donut cake tiramisu wafer. Bear claw tiramisu apple pie tart jelly beans cupcake. Chupa chups icing candy canes. Liquorice muffin jelly beans ice cream marshmallow. Pastry halvah topping liquorice. Candy cheesecake jujubes. Cake chocolate cake lemon drops. Cupcake soufflé sweet jelly caramels jelly jelly-o"
            """
        case "ICA Kvantum":
            return """
            "Chocolate bar powder gummies chocolate cake chocolate pie donut. Wafer halvah candy dragée sweet roll topping powder. Fruitcake tootsie roll bonbon toffee gummies donut cake tiramisu wafer. Bear claw tiramisu apple pie tart jelly beans cupcake. Chupa chups icing candy canes. Liquorice muffin jelly beans ice cream marshmallow. Pastry halvah topping liquorice. Candy cheesecake jujubes. Cake chocolate cake lemon drops. Cupcake soufflé sweet jelly caramels jelly jelly-o"
            """
            case "Jönköpings Tekniska Högskola":
                return """
                "Chocolate bar powder gummies chocolate cake chocolate pie donut. Wafer halvah candy dragée sweet roll topping powder. Fruitcake tootsie roll bonbon toffee gummies donut cake tiramisu wafer. Bear claw tiramisu apple pie tart jelly beans cupcake. Chupa chups icing candy canes. Liquorice muffin jelly beans ice cream marshmallow. Pastry halvah topping liquorice. Candy cheesecake jujubes. Cake chocolate cake lemon drops. Cupcake soufflé sweet jelly caramels jelly jelly-o"
                """
        default:
            return "No text yet!"
        }
    }
}
