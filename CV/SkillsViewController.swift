//
//  SkillsViewController.swift
//  CV
//
//  Created by Emil Christoffersson on 2019-11-05.
//  Copyright © 2019 Emil Christoffersson. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

   
    
    @IBAction func gobackButton(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var SkillsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.SkillsLabel.alpha = 0
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1.5, animations: {
            self.SkillsLabel.alpha = 1
        }, completion: {(true) in })


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}
