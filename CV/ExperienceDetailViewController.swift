//
//  ExperienceDetailViewController.swift
//  CV
//
//  Created by Emil Christoffersson on 2019-11-05.
//  Copyright © 2019 Emil Christoffersson. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var ExperienceImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var informationLabel: UILabel!
    
    
    var workImage: UIImage?
    var workTitle: String?
    var workDate: String?
    var workInformation: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        ExperienceImage.image = workImage
        titleLabel.text = workTitle
        dateLabel.text = workDate
        informationLabel.text = workInformation
    }
    

   

}
