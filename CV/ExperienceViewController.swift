//
//  ExperienceViewController.swift
//  CV
//
//  Created by Emil Christoffersson on 2019-10-29.
//  Copyright © 2019 Emil Christoffersson. All rights reserved.
//

import UIKit

class ExperienceViewController: UITableViewController {
    
    
    override func viewDidLoad() {
          super.viewDidLoad()
      
      }

    var Objects = [
      
            [
                 CellData(title:"MIO Möbler", date: "2018", image: UIImage.self(named:"MIOMINMIO")),
                 CellData(title:"Ystad Saltsjöbad", date: "2017", image: UIImage.self(named:"ystad")),
                 CellData(title: "ICA Kvantum", date: "2016", image: UIImage.self(named: "ica"))
            ],
            [
                CellData(title: "Jönköpings Tekniska Högskola", date: "2018-current", image: UIImage.self(named: "ju"))
            ]

       
    
    
    ]
    
    let Headers = ["Work", "Education"]
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Headers.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Objects[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
      
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for:indexPath) as? ExperienceTableViewCell{
            cell.imageViewCell?.image = self.Objects[indexPath.section][indexPath.row].imageInCell();
            cell.titleLabel.text = self.Objects[indexPath.section][indexPath.row].titleInCell()
            cell.dateLabel.text = self.Objects[indexPath.section][indexPath.row].dateInCell()
            return cell;
        }
        
        return UITableViewCell();
        
    /*    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        
        let text = Objects[indexPath.section][indexPath.row]
        
        cell.textLabel?.text = text.title
        cell.detailTextLabel?.text = text.date
        cell.imageView?.image = UIImage(named: text.image)
        return cell*/
    }
  
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Headers[section]
       }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myIndex = Objects[indexPath.section][indexPath.item]
        performSegue(withIdentifier: "SegwayPath", sender: myIndex)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? ExperienceDetailViewController
        let segwaySender: CellData? = sender as? CellData
        destination?.workImage = segwaySender?.imageInCell()
        destination?.workTitle = segwaySender?.titleInCell()
        destination?.workDate = segwaySender?.dateInCell()
        destination?.workInformation = segwaySender?.descriptionInCell()
    }
    
  
}
