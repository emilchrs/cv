//
//  ExperienceTableViewCell.swift
//  CV
//
//  Created by Emil Christoffersson on 2019-12-06.
//  Copyright © 2019 Emil Christoffersson. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imageViewCell: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
